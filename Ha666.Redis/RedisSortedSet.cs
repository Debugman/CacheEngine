﻿using System.Collections.Generic;

namespace Ha666.Redis
{
    public abstract class RedisSortedSet<T>
    {

        public RedisSortedSet(string key, DataType dataType)
        {
            mDataType = dataType;
            mDataKey = key;
        }

        private string mDataKey;

        private DataType mDataType;

        public int Zadd(int serverid, IEnumerable<SortField> sorts, RedisClient db = null)
        {
            db = RedisClient.GetClient(db);
            return db.Zadd((string)mDataKey, sorts, mDataType, serverid);
        }

        public int Zcard(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Zcard((string)mDataKey, serverid);
        }

        public int Zcount(int serverid, int min, int max)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Zcount((string)mDataKey, min, max, serverid);
        }

        public HashSet<string> Zrange(int serverid, int start, int stop)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Zrange((string)mDataKey, start, stop, mDataType, serverid);
        }

        public HashSet<T> ZrangeByScore<T>(int serverid, int min, bool isIncludemin, int max, bool isIncludemax, RedisClient db = null)
        {
            db = RedisClient.GetClient(db);
            return db.ZrangeByScore<T>((string)mDataKey, min, isIncludemin, max, isIncludemax, mDataType, serverid);
        }

        public HashSet<string> Zrevrange(int serverid, int start, int stop)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Zrevrange((string)mDataKey, start, stop, mDataType, serverid);
        }

        public int Zrank(int serverid, string member)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Zrank((string)mDataKey, member, serverid);
        }

        public int Zrevrank(int serverid, string member)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Zrevrank((string)mDataKey, member, serverid);
        }

        public void Zrem(int serverid, int member)
        {
            RedisClient db = RedisClient.GetClient(null);
            db.Zrem((string)mDataKey, member, serverid);
        }

        public int Zscore(int serverid, string member)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Zscore((string)mDataKey, member, serverid);
        }

    }

    public class StringSortedSet : RedisSortedSet<string>
    {
        public StringSortedSet(string key)
            : base(key, DataType.String)
        {
        }
    }

    public class ProtobufSortedSet<T> : RedisSortedSet<T>
    {
        public ProtobufSortedSet(string key)
            : base(key, DataType.Protobuf)
        {
        }
    }

}
