﻿namespace Ha666.Redis
{
    public class NameType
    {
        public NameType(string key, int index)
        {
            Name = key;
            Index = index;
        }
        public int Index;
        public string Name;
    }
}
