﻿using System;

namespace CacheEngine
{
    /// <summary>缓存引擎接口</summary>
    public interface ICacheEngine
    {
        /// <summary>根据关键字取得缓存数据</summary>
        /// <param name="key">关键字</param>
        /// <returns>缓存数据</returns>
        T Get<T>(string key);
        /// <summary>保存缓存数据</summary>
        /// <param name="key">关键字</param>
        /// <param name="itemToCache">缓存数据</param>
        /// <param name="expirationDate">过期时间</param>
        void SaveOrUpdate(string key, object itemToCache, DateTime expirationDate);
    }
}
